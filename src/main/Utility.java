package main;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Utility {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private double monthlyPrice;
	private String name, description, type, category;

	private int provId;
	


	public Utility() {

	}
	
	

	
	public Utility(double monthlyPrice, String name, String description, int provId, String type, String category) {
		
		this.monthlyPrice = monthlyPrice;
		this.name = name;
		this.description = description;
		this.provId = provId;
		this.type = type;
		this.category = category;
		
	}


	


	public double getMonthlyPrice() {
		return monthlyPrice;
	}



	public void setMonthlyPrice(double monthlyPrice) {
		this.monthlyPrice = monthlyPrice;
	}



	public String getName() {
		return name;
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getProvId() {
		return provId;
	}



	public void setProvId(int provId) {
		this.provId = provId;
	}




	public String getType() {
		return type;
	}




	public void setType(String type) {
		this.type = type;
	}


	


	public String getCategory() {
		return category;
	}




	public void setCategory(String category) {
		this.category = category;
	}




	@Override
	public String toString() {
		return "Utility [id=" + id + ", monthlyPrice=" + monthlyPrice + ", name=" + name + ", description="
				+ description + ", type=" + type + ", category=" + category + ", provId=" + provId + "]";
	}




	



	

	
}
