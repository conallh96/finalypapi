package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CustomerDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public CustomerDAO() {
		
	}
	
	public void persistCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(customer);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(customer);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeCustomer(Customer customer) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(customer));
		em.getTransaction().commit();
		em.close();
	}

	public List<Customer> getAllCustomers(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Customer> users = new ArrayList<Customer>();
		users = em.createQuery("from Customer").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return users;
	}
	
	public List<Customer> getAllCustomersDemo(String demo){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Customer> users = new ArrayList<Customer>();
		users = em.createQuery("from Customer where demographic = :demo").setParameter("demo", demo).getResultList();
		em.getTransaction().commit();
		em.close();
		
		return users;
	}
	
	
	public Customer getCustomerByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.name = :name", Customer.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}
	
	
	public Customer getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}
	
	public Double getSpendById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		UtilityDAO uDao = new UtilityDAO();
		
		List<CustomerUtil> custUtils = cUDao.getCustomerUtilByCustId(customer.getId());
		
		ArrayList<Utility> customersUtils = new ArrayList<>();
		
		for (CustomerUtil cU : custUtils) {
			customersUtils.add(uDao.getById(cU.getUtilityId()));
		}
		
		double total = 0;
		for( Utility util : customersUtils) {
			total = total + util.getMonthlyPrice();
		}
		
		
		
		return total;
	}
	
	public ArrayList<Utility> getUtilitiesById(int id) {
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		UtilityDAO uDao = new UtilityDAO();
		
		List<CustomerUtil> custUtils = cUDao.getCustomerUtilByCustId(customer.getId());
		
		ArrayList<Utility> customersUtils = new ArrayList<>();
		
		for (CustomerUtil cU : custUtils) {
			customersUtils.add(uDao.getById(cU.getUtilityId()));
		}
		
		
		
		
		
		return customersUtils;
	}
	
	
	public Boolean isSubbedToCat(String cat, int id) {
		
		boolean subbed = false;
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		UtilityDAO uDao = new UtilityDAO();
		
		List<CustomerUtil> custUtils = cUDao.getCustomerUtilByCustId(customer.getId());
		
		ArrayList<Utility> customersUtils = new ArrayList<>();
		
		for (CustomerUtil cU : custUtils) {
			customersUtils.add(uDao.getById(cU.getUtilityId()));
		}
		
		for (Utility util : customersUtils) {
			
			if (util.getCategory().equals(cat)){
				subbed = true;
				
			}
			
		}
		
	
		return subbed;
	}
	
	public Boolean isSubbedToUtil(int utilId, int id) {
		
		boolean subbed = false;
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		UtilityDAO uDao = new UtilityDAO();
		
		List<CustomerUtil> custUtils = cUDao.getCustomerUtilByCustId(customer.getId());
		
		ArrayList<Utility> customersUtils = new ArrayList<>();
		
		for (CustomerUtil cU : custUtils) {
			customersUtils.add(uDao.getById(cU.getUtilityId()));
		}
		
		for (Utility util : customersUtils) {
			
			if (util.getId()==(utilId)){
				subbed = true;
			}
			
		}
		
	
		return subbed;
	}
	
	
	public Customer getByEmail(String email) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Customer customer = em.createQuery("SELECT c FROM Customer c WHERE c.email = :email", Customer.class)
                .setParameter("email", email)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customer;
	}

}
