package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UtilityDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public UtilityDAO() {
		
	}
	
	public void persistUtility(Utility utility) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(utility);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeUtility(Utility utility) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(utility);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeUtility(Utility utility) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(utility));
		em.getTransaction().commit();
		em.close();
	}

	public List<Utility> getAllUtilities(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Utility> utilities = new ArrayList<Utility>();
		utilities = em.createQuery("from Utility").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return utilities;
	}
	
	public List<Utility> getSystemUtilities(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Utility> utilities = new ArrayList<Utility>();
		utilities = em.createQuery("from Utility where type = 'System'").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return utilities;
	}
	
	public List<Utility> getUtilitiesByCategory(String category){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Utility> utilities = new ArrayList<Utility>();
		utilities = em.createQuery("from Utility where type = 'System' and category = '"+category+"'").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return utilities;
	}
	
	
	public Utility getUtilityByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Utility utility = em.createQuery("SELECT u FROM Utility u WHERE u.name = :name", Utility.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return utility;
	}
	
	
	public Utility getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Utility utility = em.createQuery("SELECT u FROM Utility u WHERE u.id = :id", Utility.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return utility;
	}

}
