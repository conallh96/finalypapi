package main;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;


@Path ("/customerutilwebservice")
public class CustomerUtilWebService {
	
	CustomerUtilDAO dao = new CustomerUtilDAO();
	CustomerDAO daoC = new CustomerDAO();
	ProviderDAO daoP = new ProviderDAO();
	UtilityDAO daoU = new UtilityDAO();
	
	@GET
	@Path ("/customerutils")
	@Produces(MediaType.TEXT_HTML)
	public String getUtils() {
		
		String allItems = null;
		for (CustomerUtil util : dao.getAllCustomerUtils()) {
			allItems = allItems + "," + util.toString();
		}
		return allItems;
	}
	
/**	@POST
	@Path("/addexamplecustomerutil")
	@Produces("text/plain")
	public String addExample() {
		
		Provider prov2 = new Provider("Revolut");
		Customer steve = new Customer("Steve Smith", "smithy@gmail.com", "132 Evergreen Terrace, Bray","0864782934");
		dao.persistCustomerUtil(new CustomerUtil(24, steve, new Utility(13.99,"Revolut Premium", "Revolut Preium Membership", )));
		return "Placeholder Customer Utility saved";
	}
	
**/
	
	@DELETE
	@Path("/removeId/{id}")
	@Produces(MediaType.TEXT_HTML)
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeCustomerUtil(dao.getById(id));
		return "Removed Customer Utility with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	@Produces(MediaType.TEXT_HTML)
	public String deleteByName(@PathParam("name") String name) {
		
		dao.removeCustomerUtil(dao.getCustomerUtilByName(name));
		return "Removed Utility with name: " + name;
	}
	
	
	@GET
	@Path("/utilId/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerUtil getCustomerUtilityById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	@GET
	@Path("/myutils/{id}")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomerUtilsById(@PathParam("id")int id) {
		
		List<CustomerUtil> utils = dao.getCustomerUtilByCustId(id);
		List<Utility> utilities = new ArrayList<Utility>();
		UtilityDAO uDao = new UtilityDAO();
		String allItems = "";
		
		for (CustomerUtil util : utils) {
			
			utilities.add(uDao.getById(util.getUtilityId()));
			
		}
		
		for (Utility util : utilities) {
			
			Gson gson = new Gson();
			String utilString = gson.toJson(util);
			
			if(allItems.equals("")){
				
				allItems = allItems + utilString;
				
				}
				else if(!allItems.contains(utilString)){
					
					allItems = allItems + "/" + utilString;
					
				}
		}
			
		return allItems;
	
		}
	
	@GET
	@Path("/mycategories/{id}")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomerCatsById(@PathParam("id")int id) {
		
		List<CustomerUtil> utils = dao.getCustomerUtilByCustId(id);
		List<Utility> utilities = new ArrayList<Utility>();
		UtilityDAO uDao = new UtilityDAO();
		String allItems = "";
		
		for (CustomerUtil util : utils) {
			
			utilities.add(uDao.getById(util.getUtilityId()));
			
		}
		
		for (Utility util : utilities) {
			
			
			String utilString = util.getCategory();
			
			if(allItems.equals("")){
				
				allItems = allItems + utilString;
				
				}
				else {
					
					allItems = allItems + "/" + utilString;
					
				}
		}
			
		return allItems;
	
		}
	
	
	@GET
	@Path("/myutilsids/{id}")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomerUtilsIdsById(@PathParam("id")int id) {
		
		List<CustomerUtil> utils = dao.getCustomerUtilByCustId(id);
		
		
		String allItems = "";
		
		for (CustomerUtil util : utils) {
			
			if(allItems.equals("")){
				
				allItems = allItems + util.getId();
				
				}
				else {
					
					allItems = allItems + "/" + util.getId();
					
				}
			
		}
		
		return allItems;
	}
	
		
		@GET
		@Path("/myproviders/{id}")
		@Produces(MediaType.TEXT_HTML)
		public String getCustomerProvsById(@PathParam("id")int id) {
			
			List<CustomerUtil> utils = dao.getCustomerUtilByCustId(id);
			List<Utility> utilities = new ArrayList<Utility>();
			UtilityDAO uDao = new UtilityDAO();
			String allProvs = "";
			
			for (CustomerUtil util : utils) {
				
				utilities.add(uDao.getById(util.getUtilityId()));
				
			}
			
			for (Utility util : utilities) {
				
				ProviderDAO pDao = new ProviderDAO();
				String name = "";
				
				name = pDao.getById(util.getProvId()).getName();
				
				if(allProvs.equals("")){
					
					allProvs = allProvs + name;
					
					}
					else {
						
						allProvs = allProvs + "/" + name;
						
					}
				
		
			}
		
		return allProvs;
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerUtil createCustomerUtil(CustomerUtil util) {
		
		dao.persistCustomerUtil(util);
		return util;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public CustomerUtil updateCustomerUtil( CustomerUtil util,@PathParam("id") int id) {
		
		util.setId(id);
		dao.mergeCustomerUtil(util);
		
		return util;
		
	}

	

}