package main;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestDatabase {
	

	
	public TestDatabase() {
		
		CustomerUtilDAO dao = new CustomerUtilDAO();
		CustomerDAO daoC = new CustomerDAO();
		ProviderDAO daoP = new ProviderDAO();
		UtilityDAO daoU = new UtilityDAO();
		
		scrapeBroadband();
		scrapeSimOnly();
		//scrapeTv();
		
		Provider prov2 = new Provider("Sony");
		daoP.persistProvider(prov2);
		
		Utility utility = new Utility(8.99,"Playstation Plus", "Playstation Plus Monthly Membership", daoP.getProviderByName(prov2.getName()).getId(), "User", "Gaming");
		
		daoU.persistUtility(utility);
	
		Customer phil = new Customer("Phil Rogers", "rogerthat@gmail.com", "23 Sycamore Terrace" , "Deansgrange", "Dublin", "A94 K3Z4","none","0854783254", 2050.00,"Customer");
		daoC.persistCustomer(phil);
		
		CustomerUtil util = new CustomerUtil(utility.getName(),daoC.getCustomerByName(phil.getName()).getId(), daoU.getUtilityByName(utility.getName()).getId(),22);
		
		
		
		dao.persistCustomerUtil(util);

	}
	
	
	public void scrapeBroadband() {
		
        final StringBuilder builder = new StringBuilder();
      

        try {
            Document doc = Jsoup.connect("https://www.moneyguideireland.com/cheapest-phone-and-broadband-package.html").get();
            String title = doc.title();
            builder.append(title).append("\n");


            Elements limit = doc.getElementsByClass("column-6");
            Elements price = doc.getElementsByClass("column-4");
            Elements provider = doc.getElementsByClass("column-1");
            Elements speed = doc.getElementsByClass("column-5");
            


            for (int i = 1; i < limit.size() ; i ++) {

     
            	String priceString = price.get(i).text();
            	String[] priceSplit = priceString.split(" ");
            	String purePrice = priceSplit[0].replace("€", "");
            	double priceValue = Double.parseDouble(purePrice);
            	            	
            	String descriptionString = "Speeds of up to: " + speed.get(i).text();
            	
            	String providerString = provider.get(i).text();
            	
            	String nameString = provider.get(i).text() + " " +limit.get(i).text();
            	
                ProviderDAO pDao = new ProviderDAO();
                UtilityDAO daoUtil = new UtilityDAO();
                
                if (pDao.getExistsByName(providerString) == true) {
                	
                	Utility utility = new Utility(priceValue, nameString , descriptionString,pDao.getProviderByName(providerString).getId(), "System", "Broadband");	
                	
                	if(utility.getMonthlyPrice()>0) {
            			
                    	daoUtil.persistUtility(utility);
                    	System.out.println(utility.toString());
                    	
                		}
                }
                
                else {
                	
                pDao.persistProvider( new Provider(providerString));
                
        		Utility utility = new Utility(priceValue, nameString , descriptionString, pDao.getProviderByName(providerString).getId(), "System", "Broadband");
        		
        		if(utility.getMonthlyPrice()>0) {
        			
                	daoUtil.persistUtility(utility);
                	System.out.println(utility.toString());
                	
            		}
                }
            	
            }



        } catch (IOException e) {
            builder.append("Error : ").append(e.getMessage()).append("\n");
        }
        
        
	}
	
	public void scrapeSimOnly() {
		
    
        try {
            Document doc = Jsoup.connect("https://www.moneyguideireland.com/irish-mobile-sim-only-plans-compared.html").get();
            String title = doc.title();
            
            int counter = 0;
            
            Elements rows = doc.select("tr");
            for(Element row :rows)
            {
            	
         		double priceValue = 0;
        		String providerString = "";
        		String descriptionString = "";
        		String priceString = "";
        		String[] providerSplit;
        		String[] priceSplit;
        		UtilityDAO daoUtil = new UtilityDAO();
        		
                Elements columns = row.select("td");
                for (int i = 0; i < columns.size(); i++)
                {
                	

            		
            		if (counter > 0) {
            		
                    if (i == 0){
                    	
                    	providerString = columns.get(i).text();
                    	
                    	if (providerString.contains("(")) {
                    		
                        	providerSplit = providerString.split("\\(");
                        	providerString = providerSplit[0];
                    		
                    	}
                    	
                    	if (providerString.contains("#")) {
                    		
                        	providerSplit = providerString.split("#");
                        	providerString = providerSplit[0];
                    		
                    	}
                    	
                    	
                    	
                    }
                    
                    if (i == 1){
                    	
                    	
                    	
                    	priceString = columns.get(i).text();
                    	
                    	
                    	
                    	String purePrice = priceString.replace("€", "");
                    	
                    	
                    	
                    	if ( purePrice.contains(" ")) {
                    		
                    		priceSplit = purePrice.split(" ");
                        	purePrice = priceSplit[0];
                        	
                    		
                    	}
                    	
                    	priceValue = Double.parseDouble(purePrice);
                   
                    	
                    }
                    
                    if (i == 2){
                    	descriptionString = "Description: Minutes: " + columns.get(i).text() + " Texts: "+ columns.get(i+1).text() + " Data: "+ columns.get(i+2).text();
                    }
                    
                    
                    
                }
            		
            
                
                }
                
                
                ProviderDAO pDao = new ProviderDAO();
                
                if (pDao.getExistsByName(providerString) == true) {
                	
                	Utility utility = new Utility(priceValue, providerString+ " Sim-Only" , descriptionString,pDao.getProviderByName(providerString).getId(), "System", "Mobile Plan");	
                	
                	if(utility.getMonthlyPrice()>0) {
            			
                    	daoUtil.persistUtility(utility);
                    	System.out.println(utility.toString());
                    	
                		}
                }
                
                else {
                	
                pDao.persistProvider( new Provider(providerString));
                
        		Utility utility = new Utility(priceValue, providerString+ " Sim-Only" , descriptionString,pDao.getProviderByName(providerString).getId(), "System", "Mobile Plan");
        		
        		if(utility.getMonthlyPrice()>0) {
        			
                	daoUtil.persistUtility(utility);
                	System.out.println(utility.toString());
                	
            		}
                }
            	
        		
            	
                counter++;
            }
            



        } catch (IOException e) {
            System.out.println("Error : " +e.getMessage()+ "\n");
        }
        
        
	}
	
	public void scrapeTv() {
		

        final StringBuilder builder = new StringBuilder();
      

        try {
            Document doc = Jsoup.connect("https://www.bonkers.ie/compare-tv-broadband-phone/s/cheapest-tv-deals-in-ireland/").get();
            String title = doc.title();
            builder.append(title).append("\n");


            
  
            Elements names = doc.select("h4");
            Elements providers = doc.getElementsByClass("result-provider__logo");
            Elements prices = doc.getElementsByClass("price");
            Elements imgs =  providers.select("img");
            


            for (int i = 0; i < names.size() ; i++) {

     
            	String nameString = names.get(i).text();
            	String alt = imgs.get(i).attr("alt");
            	String priceString  = prices.get(i).text();
            	
            	            	

            	System.out.println(nameString + " by " + alt + " costs " + priceString);
            }



        } catch (IOException e) {
            builder.append("Error : ").append(e.getMessage()).append("\n");
        }
        
        
		
	}
	

	public static void main(String[] args) {
		new TestDatabase();
	}

}