package main;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class AddBundles {

	
	public AddBundles() {
		scrapeBundles();
	}
	
	public void scrapeBundles() {
		
        final StringBuilder builder = new StringBuilder();
      

        try {
            Document doc = Jsoup.connect("https://switcher.ie/broadband/compare/broadband-and-digital-tv/").get();
        


            Elements detailsAll = doc.getElementsByClass("c-result-row__plan-detail__item");
            ArrayList<String> details = (ArrayList<String>) detailsAll.eachText();
            
            Elements namesAll = doc.getElementsByClass("c-result-row__plan-name");
            ArrayList<String> names = (ArrayList<String>) namesAll.eachText();
            
            Elements imagesAll = doc.getElementsByAttribute("alt");
           
            ArrayList<String> providers = new ArrayList<>();
            ArrayList<String> speeds = new ArrayList<>();
            ArrayList<String> techs = new ArrayList<>();
            ArrayList<String> channels = new ArrayList<>();
            ArrayList<String> datas = new ArrayList<>();
            ArrayList<String> contracts = new ArrayList<>();
            ArrayList<Double> prices = new ArrayList<>();
            
            
            for (int i = 0; i < imagesAll.size(); i++) {
            	
            	providers.add(imagesAll.get(i).attr("alt"));
            	
            }
            
       
            for (int i = 0 ; i < details.size(); i++) {
            	
            	if (i== 0|| i%6 == 0) {
            		details.set(i, "Speed of up to: "+  details.get(i));
            		speeds.add(details.get(i));
            	}
            	
            	
            	if (i%6 == 1) {
            		details.set(i, "Technnology: "+  details.get(i));
            		techs.add(details.get(i));
            	}
            	
            	if (i%6 == 2) {
            		details.set(i, "Channels: "+  details.get(i));
            		channels.add(details.get(i));
            	}


            	if (i%6 == 3) {
            		details.set(i, "Data Allowance: "+  details.get(i));
            		datas.add(details.get(i));
            	}
            	
            	if (i%6 == 4) {
            		details.set(i, "Contract Length: "+  details.get(i));
            		contracts.add(details.get(i));
            	}
            	
            	if (i%6 == 5) {
            		details.set(i,details.get(i).split("p")[0]);
            		prices.add(Double.valueOf(details.get(i).split("€")[1]));
            	}

            	
            }
            
            System.out.println("DETAIL LIST" + details.toString());
            System.out.println("DETAIL LIST BUNDLES " + details.size()/6);
            System.out.println("Names " + names.size());
            System.out.println("Providers " +  providers.size());
            System.out.println("Channels " +  channels.size());
            System.out.println("Prices " +  prices.size());
            System.out.println("Data Allowances " +  datas.size());
            System.out.println("Contracts " +  contracts.size());
            System.out.println("Speeds " +  speeds.size());
            
            for (int i = 0; i < names.size(); i++) {
            	
            	
            	 ProviderDAO pDao = new ProviderDAO();
                 UtilityDAO daoUtil = new UtilityDAO();
                 
                 if (pDao.getExistsByName(providers.get(i)) == true) {
                 	
                 	Utility utility = new Utility(prices.get(i), names.get(i) , speeds.get(i)+". "+channels.get(i)+". "+ datas.get(i),pDao.getProviderByName(providers.get(i)).getId(), "System", "Bundle");	
                 	
                 	if(utility.getMonthlyPrice()>0) {
             			
                     	daoUtil.persistUtility(utility);
                     	System.out.println(utility.toString());
                     	
                 		}
                 }
                 
                 else {
                 	
                 pDao.persistProvider( new Provider(providers.get(i)));
                 
         		Utility utility = new Utility(prices.get(i), names.get(i) , speeds.get(i)+". "+channels.get(i)+". "+ datas.get(i), pDao.getProviderByName(providers.get(i)).getId(), "System", "Bundle");
         		
         		if(utility.getMonthlyPrice()>0) {
         			
                 	daoUtil.persistUtility(utility);
                 	System.out.println(utility.toString());
                 	
             		}
                 }
             	
             }

  
        } catch (IOException e) {
            builder.append("Error : ").append(e.getMessage()).append("\n");
        }
        
        
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new AddBundles();
	}

}
