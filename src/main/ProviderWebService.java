package main;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path ("/providerwebservice")
public class ProviderWebService {
	
	ProviderDAO dao = new ProviderDAO(); 
	
	@GET
	@Path ("/providers")
	@Produces(MediaType.TEXT_HTML)
	public String getProviders() {
		
		 
		String allProviders = null;
		for (Provider prov : dao.getAllProviders()) {
			allProviders = allProviders + "," + prov.toString();
		}
		return allProviders;
	}
	
	@POST
	@Path("/addexampleutil")
	@Produces("text/plain")
	public String addExample() {
		
		Provider sky = new Provider("Sky");
		dao.persistProvider(sky);
		return "Placeholder Provider saved";
	}
	

	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeProvider(dao.getById(id));
		return "Removed Provider with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	public String deleteByName(@PathParam("name") String name) {
		
		dao.removeProvider(dao.getProviderByName(name));
		return "Removed Utility with name: " + name;
	}
	
	
	@GET
	@Path("/providerNameById/{id}")
	@Produces({MediaType.TEXT_HTML})
	public String nameById(@PathParam("id")int id) {
		
		return dao.getById(id).getName();
	}
	
	
	@GET
	@Path("/providerId/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Provider getById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Provider create(Provider prov) {
		
		if (dao.getExistsByName(prov.getName())) {
			
			return dao.getProviderByName(prov.getName());
			
		}
		
		
		else {
			
		dao.persistProvider(prov);
		return prov;
		
		}
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Provider update( Provider prov,@PathParam("id") int id) {
		
		prov.setId(id);
		dao.mergeProvider(prov);
		return  prov;
	}

	

}