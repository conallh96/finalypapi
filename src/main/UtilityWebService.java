package main;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;



@Path ("/utilwebservice")
public class UtilityWebService {
	
	@GET
	@Path ("/utilities")
	@Produces("text/plain")
	public String getUtils() {
		UtilityDAO dao = new UtilityDAO();
		String allItems = "";
		for (Utility util : dao.getAllUtilities()) {
			
			Gson gson = new Gson();
			String utilString = gson.toJson(util);
			
			if(allItems.equals("")){
			
			allItems = allItems + utilString;
			
			}
			else {
				
				allItems = allItems + "/" + utilString;
				
			}
	
		}
		return  allItems;
	}
	
	
	@GET
	@Path("/subscribedToUtil/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Double getSpendByDemo( @PathParam("id")int id ) {
		
		int yes = 0;
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		ArrayList<Customer> customers = (ArrayList<Customer>) cDao.getAllCustomers();
		
		
		
		
		for (Customer customer : customers) {
			
			
			if( cDao.isSubbedToUtil(id, customer.getId()) ) {
				yes = yes + 1;
			}
			
		}
		
		
		
		double percentSubbed = (double)((double)yes/(double)customers.size())*100;
		
	
		
		return percentSubbed;
	}
	
	
	@GET
	@Path ("/utilsbycat/{cat}")
	@Produces("text/plain")
	public String getUtilsByCat(@PathParam("cat") String cat) {
		UtilityDAO dao = new UtilityDAO();
		String allItems = "";
		for (Utility util : dao.getUtilitiesByCategory(cat)) {
			
			Gson gson = new Gson();
			String utilString = gson.toJson(util);
			
			if(allItems.equals("")){
			
			allItems = allItems + utilString;
			
			}
			else {
				
				allItems = allItems + "/" + utilString;
				
			}
	
		}
		return  allItems;
	}
	
	@GET
	@Path ("/utilsprovsbycat/{cat}")
	@Produces("text/plain")
	public String getUtilsProvsByCat(@PathParam("cat") String cat) {
		UtilityDAO dao = new UtilityDAO();
		String allProvs = "";
		for (Utility util : dao.getUtilitiesByCategory(cat)) {
			
			ProviderDAO pDao = new ProviderDAO();
			String name = "";
			
			name = pDao.getById(util.getProvId()).getName();
			
			if(allProvs.equals("")){
				
				allProvs = allProvs + name;
				
				}
				else {
					
					allProvs = allProvs + "/" + name;
					
				}
	
		}
		return  allProvs;
	}
	
	@GET
	@Path ("/systemutilities")
	@Produces("text/plain")
	public String getSystemUtils() {
		UtilityDAO dao = new UtilityDAO();
		String allItems = "";
		for (Utility util : dao.getSystemUtilities()) {
			
			Gson gson = new Gson();
			String utilString = gson.toJson(util);
			
			if(allItems.equals("")){
			
			allItems = allItems + utilString;
			
			}
			else {
				
				allItems = allItems + "/" + utilString;
				
			}
	
		}
		return  allItems;
	}
	
	@GET
	@Path ("/utilityprovs")
	@Produces("text/plain")
	public String getProvs() {
		UtilityDAO dao = new UtilityDAO();
		String allProvs = "";
		for (Utility util : dao.getAllUtilities()) {
			
			ProviderDAO pDao = new ProviderDAO();
			String name = "";
			
			name = pDao.getById(util.getProvId()).getName();
			
			if(allProvs.equals("")){
				
				allProvs = allProvs + name;
				
				}
				else {
					
					allProvs = allProvs + "/" + name;
					
				}
	
		}
		return  allProvs;
	}
	
	@GET
	@Path ("/mostPopUtil")
	@Produces("text/plain")
	public String getPops() {
		UtilityDAO udao = new UtilityDAO();
		CustomerUtilDAO cUtilDao = new CustomerUtilDAO();
	
		String utilString ="";
		
		HashMap<String, Integer> utilCount = new HashMap<String,Integer>();
		
		ArrayList<CustomerUtil> cUtils = (ArrayList<CustomerUtil>) cUtilDao.getAllCustomerUtils();
		
		for (int i = 0; i < cUtils.size(); i++){
			
			 String number = String.valueOf(cUtils.get(i).getUtilityId());
				 
				 if (utilCount.containsKey(number)) {
					 utilCount.replace(number, utilCount.get(number)+1);
				 }
				 else {
					 utilCount.put(number,1);
				 }
				 
			 
		 }
		
		
			 Map.Entry<String, Integer> maxEntry = null;
			
			 for (Map.Entry<String, Integer> entry : utilCount.entrySet())
			 {
			     if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
			     {
			         maxEntry = entry;
			     }
			 }
			 
			 Gson gson = new Gson();
			 utilString = gson.toJson(udao.getById(Integer.parseInt(maxEntry.getKey())));
			 
		return  utilString;
	}
	
	
	@GET
	@Path ("/systemutilityprovs")
	@Produces("text/plain")
	public String getSystemProvs() {
		UtilityDAO dao = new UtilityDAO();
		String allProvs = "";
		for (Utility util : dao.getSystemUtilities()) {
			
			ProviderDAO pDao = new ProviderDAO();
			String name = "";
			
			name = pDao.getById(util.getProvId()).getName();
			
			if(allProvs.equals("")){
				
				allProvs = allProvs + name;
				
				}
				else {
					
					allProvs = allProvs + "/" + name;
					
				}
	
		}
		return  allProvs;
	}
	
	@GET
	@Path ("/systemcategories")
	@Produces("text/plain")
	public String getSystemCats() {
		UtilityDAO dao = new UtilityDAO();
		String allProvs = "";
		for (Utility util : dao.getSystemUtilities()) {
			
			
			
			
			String name = util.getCategory();
			
			if(allProvs.equals("")){
				
				allProvs = allProvs + name;
				
				}
				else if(!allProvs.contains(name)) {
					
					allProvs = allProvs + "/" + name;
					
				}
	
		}
		return  allProvs;
	}
	

	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		UtilityDAO dao = new UtilityDAO();
		dao.removeUtility(dao.getById(id));
		return "Removed Utility with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	public String deleteByName(@PathParam("name") String name) {
		UtilityDAO dao = new UtilityDAO();
		dao.removeUtility(dao.getUtilityByName(name));
		return "Removed Utility with name: " + name;
	}
	
	
	@GET
	@Path("/utilId/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Utility getUtilityById(@PathParam("id")int id) {
		
		UtilityDAO dao = new UtilityDAO();
		
		return dao.getById(id);
	}
	
	@GET
	@Path("/suggestCategory/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String getCategoryById(@PathParam("id")int id) {
		
		String result = "";
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		Customer cust = cDao.getById(id);
		ArrayList<Customer> sameDemo = (ArrayList<Customer>) cDao.getAllCustomersDemo(cust.getDemographic());
		String customerCats = "";
		String myCats = cUDao.getCustomerCatsById(id);
		
		
		for (Customer customer : sameDemo) {
			
			customerCats = customerCats + cUDao.getCustomerCatsById(customer.getId());
			
		}
		
		String categories[] = {""};
		
		if (customerCats.contains("-")) {
			
		
			categories = customerCats.split("-");
		
		}
		else {
			categories[0]=customerCats;
		}
		
		 HashMap<String, Integer> popularCats = new HashMap<String,Integer>();
		 
		 for (int i = 0 ; i < categories.length; i++) {
			 
			 if (popularCats.containsKey(categories[i])) {
				 popularCats.replace(categories[i], popularCats.get(categories[i])+1);
			 }
			 else {
				 popularCats.put(categories[i],1);
			 }
			 
		 }
		 
		Iterator it = popularCats.entrySet().iterator();
		HashMap<String, Integer> missingCats = new HashMap<String,Integer>();
		
		while(it.hasNext()) {
			
			Map.Entry<String, Integer> pair = (Map.Entry)it.next();	
			
			if (!myCats.contains(pair.getKey())) {
				missingCats.put(pair.getKey(),pair.getValue());
			}
		}
		
		Iterator ite = missingCats.entrySet().iterator();
		
		while(ite.hasNext()) {
			
			String max = "";
			int high = 0;
			
			Map.Entry<String, Integer> pair = (Map.Entry)ite.next();
			if (pair.getValue()>high) {
				max = pair.getKey();
			}
			
			result = max;
		
		}
		
		return result;
	}
	
	@GET
	@Path("/catsDemo/{demo}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String getCatsByDemo(@PathParam("demo")String demo) {
		
		String result = "";
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		ArrayList<Customer> sameDemo = (ArrayList<Customer>) cDao.getAllCustomersDemo(demo);
		String customerCats = "";
		
		
		
		for (Customer customer : sameDemo) {
			
			customerCats = customerCats + cUDao.getCustomerCatsById(customer.getId());
			
		}
		
		
		
		return customerCats;
	}
	
	
	@GET
	@Path("/averageSpendDemo/{demo}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Double getSpendByDemo(@PathParam("demo")String demo) {
		
		String result = "";
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		ArrayList<Customer> sameDemo = (ArrayList<Customer>) cDao.getAllCustomersDemo(demo);
		double totalSpends = 0;
		
		
		
		for (Customer customer : sameDemo) {
			
			totalSpends = totalSpends + cDao.getSpendById(customer.getId());
			
		}
		
		double averageSpend = totalSpends/sameDemo.size();
		
		DecimalFormat df = new DecimalFormat("#.##");      
		averageSpend = Double.parseDouble(df.format(averageSpend));
		
		return averageSpend;
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Utility createUtil(Utility util) {
		UtilityDAO dao = new UtilityDAO();
		dao.persistUtility(util);
		return util;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Utility updateUtil( Utility util,@PathParam("id") int id) {
		UtilityDAO dao = new UtilityDAO();
		util.setId(id);
		dao.mergeUtility(util);
		return  util;
	}

	

}