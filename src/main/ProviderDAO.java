package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

public class ProviderDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public ProviderDAO() {
		
	}
	
	public void persistProvider(Provider provider) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(provider);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeProvider(Provider provider) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(provider);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeProvider(Provider provider) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(provider));
		em.getTransaction().commit();
		em.close();
	}

	public List<Provider> getAllProviders(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<Provider> providers = new ArrayList<Provider>();
		providers = em.createQuery("from Provider").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return providers;
	}
	
	
	public Provider getProviderByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Provider provider = em.createQuery("SELECT p FROM Provider p WHERE p.name = :name", Provider.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return provider;
	}
	
	public Boolean getExistsByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Provider provider = null;
		try {
		provider = em.createQuery("SELECT p FROM Provider p WHERE p.name = :name", Provider.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		}
		catch (NoResultException nre){
			
			
			}
		if (provider != null) {
		return true;
		}
		else {
			return false;
		}
	}
	
	
	public Provider getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Provider provider = em.createQuery("SELECT p FROM Provider p WHERE p.id = :id", Provider.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return provider;
	}

}