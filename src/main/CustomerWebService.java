package main;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


@Path ("/customerwebservice")
public class CustomerWebService {
	
	CustomerDAO dao = new CustomerDAO();
	
	@GET
	@Path ("/customer")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomers() {
		CustomerDAO dao = new CustomerDAO();
		String allItems = "";
		for (Customer cust : dao.getAllCustomers()) {
			
			Gson gson = new Gson();
			String custString = gson.toJson(cust);
			
			if(allItems.equals("")){
			
			allItems = allItems + custString;
			
			}
			else {
				
				allItems = allItems + "/" + custString;
				
			}
	
		}
		return  allItems;
	}
	
	@GET
	@Path ("/customersDemo/{demo}")
	@Produces(MediaType.TEXT_HTML)
	public String getCustomersDemo(@PathParam("demo") String demo) {
		CustomerDAO dao = new CustomerDAO();
		String allItems = "";
		for (Customer cust : dao.getAllCustomersDemo(demo)) {
			
			Gson gson = new Gson();
			String custString = gson.toJson(cust);
			
			if(allItems.equals("")){
			
			allItems = allItems + custString;
			
			}
			else {
				
				allItems = allItems + "/" + custString;
				
			}
	
		}
		return  allItems;
	}
	
	@GET
	@Path ("/biggestDemo")
	@Produces(MediaType.TEXT_HTML)
	public String getBiggestDemo() {
		CustomerDAO dao = new CustomerDAO();
		
		HashMap<String, Integer> demoSizes = new HashMap<String,Integer>();
		String catString ="";

		 
		 for (Customer cust : dao.getAllCustomers()) {
			 
			 if(!(cust.getDemographic().equals("none"))) {
				 
				 if (demoSizes.containsKey(cust.getDemographic())) {
					 demoSizes.replace(cust.getDemographic(), demoSizes.get(cust.getDemographic())+1);
				 }
				 else {
					 demoSizes.put(cust.getDemographic(),1);
				 }
				 
			 }
		 }
		
		
			 Map.Entry<String, Integer> maxEntry = null;
			
			 for (Map.Entry<String, Integer> entry : demoSizes.entrySet())
			 {
			     if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
			     {
			         maxEntry = entry;
			     }
			 }
			 
			 DemographicCount bigCat = new DemographicCount(maxEntry.getKey(),maxEntry.getValue());
		
			
			Gson gson = new Gson();
			catString = gson.toJson(bigCat);
			
			return  catString;
	
		}
	
	@GET
	@Path ("/smallestDemo")
	@Produces(MediaType.TEXT_HTML)
	public String getSmallestDemo() {
		CustomerDAO dao = new CustomerDAO();
		
		HashMap<String, Integer> demoSizes = new HashMap<String,Integer>();
		String catString ="";

		 
		 for (Customer cust : dao.getAllCustomers()) {
			 
			 if(!(cust.getDemographic().equals("none"))) {
				 
				 if (demoSizes.containsKey(cust.getDemographic())) {
					 demoSizes.replace(cust.getDemographic(), demoSizes.get(cust.getDemographic())+1);
				 }
				 else {
					 demoSizes.put(cust.getDemographic(),1);
				 }
				 
			 }
		 }
		
		
			 Map.Entry<String, Integer> minEntry = null;
			
			 for (Map.Entry<String, Integer> entry : demoSizes.entrySet())
			 {
			     if (minEntry == null || entry.getValue().compareTo(minEntry.getValue()) < 0)
			     {
			         minEntry = entry;
			     }
			 }
			 
			DemographicCount smallCat = new DemographicCount(minEntry.getKey(),minEntry.getValue());
		
			
			Gson gson = new Gson();
			catString = gson.toJson(smallCat);
			
			return  catString;
	
		}
		
	
	
	
	@GET
	@Path("/subscribedToCat/{demo}/{cat}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Double getSpendByDemo(@PathParam("cat")String cat, @PathParam("demo")String demo ) {
		
		int yes = 0;
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		ArrayList<Customer> sameDemo = (ArrayList<Customer>) cDao.getAllCustomersDemo(demo);
		
		
		
		
		for (Customer customer : sameDemo) {
			
			
			if( cDao.isSubbedToCat(cat, customer.getId()) ) {
				yes = yes + 1;
			}
			
		}
		
		
		
		double percentSubbed = (double)((double)yes/(double)sameDemo.size())*100;
		
		
		DecimalFormat df = new DecimalFormat("#.##");      
		percentSubbed = Double.parseDouble(df.format(percentSubbed));

		
	
		
		return percentSubbed;
	}
	
	
	@GET
	@Path("/marketPlayers")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String getPlayers() {
		
		
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		String results = "";
		
		HashMap<String, Integer> categories = new HashMap<String,Integer>();
		
		for(CustomerUtil cUtil : cUDao.getAllCustomerUtils()){
			
			String cat = uDao.getById(cUtil.getUtilityId()).getCategory();
			
			if (categories.containsKey(cat)) {
				categories.replace(cat, categories.get(cat)+1);
			 }
			 else {
				 categories.put(cat,1);
			 }
			
		}
		
		 
			
		 for (Map.Entry<String, Integer> entry : categories.entrySet())
		 {
		
			 results = results + entry.getKey()+"-";
			 
		 }
	
		
		return results;
	}
	
	
	@GET
	@Path("/marketShares")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String getShares() {
		
		
		UtilityDAO uDao = new UtilityDAO();
		CustomerDAO cDao = new CustomerDAO();
		CustomerUtilDAO cUDao = new CustomerUtilDAO();
		
		String results = "";
		
		HashMap<String, Integer> categories = new HashMap<String,Integer>();
		
		for(CustomerUtil cUtil : cUDao.getAllCustomerUtils()){
			
			String cat = uDao.getById(cUtil.getUtilityId()).getCategory();
			
			if (categories.containsKey(cat)) {
				categories.replace(cat, categories.get(cat)+1);
			 }
			 else {
				 categories.put(cat,1);
			 }
			
		}
		
		 
			
		 for (Map.Entry<String, Integer> entry : categories.entrySet())
		 {
		   
		
			 double percent = (double)((double)entry.getValue()/(double)cUDao.getAllCustomerUtils().size())*100;
			 results = results + percent+"-";
			 
			 
		 }
	
		
		return results;
	}
	
	
	
	@POST
	@Path("/addexamplecustomer")
	@Produces("text/plain")
	public String addExample() {
		
		
		Customer cust1 = new Customer("John Maxwell", "jmaxwell@gmail.com", "17 Pine Cottages", "Pine Forest","Wicklow","A95 K5D3","none","0865387211", 1800.00,"Customer");
		
		dao.persistCustomer(cust1);
		
		return "Placeholder Customer saved";
	}
	

	
	@DELETE
	@Path("/removeId/{id}")
	public String deleteById(@PathParam("id") int id) {
		
		dao.removeCustomer(dao.getById(id));
		return "Removed Customer with id: " + id;
	}
	
	
	
	@DELETE
	@Path("/remove/{name}")
	public String deleteByName(@PathParam("name") String name) {
		
		dao.removeCustomer(dao.getCustomerByName(name));
		return "Removed Customer with name: " + name;
	}
	
	
	@GET
	@Path("/customerId/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer getCustomerById(@PathParam("id")int id) {
		
		return dao.getById(id);
	}
	
	@GET
	@Path("/customerEmail/{email}")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer getCustomerByEmail(@PathParam("email")String email) {
		
		return dao.getByEmail(email);
	}
	
	
	@POST
	@Path("/post")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer createUtil(Customer cust) {
		
		dao.persistCustomer(cust);
		return cust;
	}
	
	@PUT
	@Path("/update/{id}")
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Customer updateUtil( Customer cust,@PathParam("id") int id) {
		
		cust.setId(id);
		dao.mergeCustomer(cust);
		return  cust;
	}

	

}