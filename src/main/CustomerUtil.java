package main;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class CustomerUtil {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	String name;
	int customerId;
	int utilityId;
	private int payDate;
	

	public CustomerUtil() {
		
	}


	


	public CustomerUtil(String name, int customerId, int utilityId, int payDate) {
		super();
		this.name = name;
		this.customerId = customerId;
		this.utilityId = utilityId;
		this.payDate = payDate;
	}





	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getPayDate() {
		return payDate;
	}


	public void setPayDate(int payDate) {
		this.payDate = payDate;
	}





	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public int getCustomerId() {
		return customerId;
	}





	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}





	public int getUtilityId() {
		return utilityId;
	}





	public void setUtilityId(int utilityId) {
		this.utilityId = utilityId;
	}





	@Override
	public String toString() {
		return "CustomerUtil [id=" + id + ", name=" + name + ", customerId=" + customerId + ", utilityId=" + utilityId
				+ ", payDate=" + payDate + "]";
	}

	
	
	
	
	

	
	
}
