package main;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.PathParam;

public class CustomerUtilDAO {
	
	protected static EntityManagerFactory emf =
			Persistence.createEntityManagerFactory("ConallsPU");
			

	public CustomerUtilDAO() {
		
	}
	
	public void persistCustomerUtil(CustomerUtil customerUtil) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(customerUtil);
		em.getTransaction().commit();
		em.close();
	}
	
	public void mergeCustomerUtil(CustomerUtil customerUtil) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.merge(customerUtil);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeCustomerUtil(CustomerUtil customerUtil) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.merge(customerUtil));
		em.getTransaction().commit();
		em.close();
	}

	public List<CustomerUtil> getAllCustomerUtils(){
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		List<CustomerUtil> utils = new ArrayList<CustomerUtil>();
		utils = em.createQuery("from CustomerUtil").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return utils;
	}
	
	
	public CustomerUtil getCustomerUtilByName(String name) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CustomerUtil customerUtil = em.createQuery("SELECT c FROM CustomerUtil c WHERE c.name = :name", CustomerUtil.class)
                .setParameter("name", name)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customerUtil;
	}
	
	// Needs to return array list of customer utils;
	
	public List<CustomerUtil> getCustomerUtilByCustId(int id) {
		EntityManager em = emf.createEntityManager();
		List<CustomerUtil> utils = new ArrayList<CustomerUtil>();
		
		em.getTransaction().begin();
		utils = em.createQuery("FROM CustomerUtil c WHERE c.customerId = :id", CustomerUtil.class)
                .setParameter("id", id)
                .getResultList();
		em.getTransaction().commit();
		em.close();
		return utils;
	}
	
		public String getCustomerCatsById(int id) {
		CustomerUtilDAO dao = new CustomerUtilDAO();
		List<CustomerUtil> utils = dao.getCustomerUtilByCustId(id);
		List<Utility> utilities = new ArrayList<Utility>();
		UtilityDAO uDao = new UtilityDAO();
		String allItems = "";
		
		for (CustomerUtil util : utils) {
			
			utilities.add(uDao.getById(util.getUtilityId()));
			
		}
		
		for (Utility util : utilities) {
			
			
			String utilString = util.getCategory();
			
			if(allItems.equals("")){
				
				allItems = allItems + utilString + "-";
				
				}
				else {
					
					allItems = allItems + utilString + "-";
					
				}
		}
			
		return allItems;
	
		}
	
	
	public CustomerUtil getById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CustomerUtil customerUtil = em.createQuery("SELECT c FROM CustomerUtil c WHERE c.id = :id", CustomerUtil.class)
                .setParameter("id", id)
                .getSingleResult();
		em.getTransaction().commit();
		em.close();
		return customerUtil;
	}

}